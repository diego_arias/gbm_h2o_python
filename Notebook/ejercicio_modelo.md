
# Modelo Python

## Librerías

Se instalan librerías


```python
import numpy as np
import pandas as pd
from dfply import * #dplyr para python
from plotnine import * # ggplot2 para python
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score
```

## Leer información


```python
data=pd.read_csv('bank-full.csv',delimiter=';')
```


```python
(
 data >>
    head()
)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>age</th>
      <th>job</th>
      <th>marital</th>
      <th>education</th>
      <th>default</th>
      <th>balance</th>
      <th>housing</th>
      <th>loan</th>
      <th>contact</th>
      <th>day</th>
      <th>month</th>
      <th>duration</th>
      <th>campaign</th>
      <th>pdays</th>
      <th>previous</th>
      <th>poutcome</th>
      <th>y</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>58</td>
      <td>management</td>
      <td>married</td>
      <td>tertiary</td>
      <td>no</td>
      <td>2143</td>
      <td>yes</td>
      <td>no</td>
      <td>unknown</td>
      <td>5</td>
      <td>may</td>
      <td>261</td>
      <td>1</td>
      <td>-1</td>
      <td>0</td>
      <td>unknown</td>
      <td>no</td>
    </tr>
    <tr>
      <th>1</th>
      <td>44</td>
      <td>technician</td>
      <td>single</td>
      <td>secondary</td>
      <td>no</td>
      <td>29</td>
      <td>yes</td>
      <td>no</td>
      <td>unknown</td>
      <td>5</td>
      <td>may</td>
      <td>151</td>
      <td>1</td>
      <td>-1</td>
      <td>0</td>
      <td>unknown</td>
      <td>no</td>
    </tr>
    <tr>
      <th>2</th>
      <td>33</td>
      <td>entrepreneur</td>
      <td>married</td>
      <td>secondary</td>
      <td>no</td>
      <td>2</td>
      <td>yes</td>
      <td>yes</td>
      <td>unknown</td>
      <td>5</td>
      <td>may</td>
      <td>76</td>
      <td>1</td>
      <td>-1</td>
      <td>0</td>
      <td>unknown</td>
      <td>no</td>
    </tr>
    <tr>
      <th>3</th>
      <td>47</td>
      <td>blue-collar</td>
      <td>married</td>
      <td>unknown</td>
      <td>no</td>
      <td>1506</td>
      <td>yes</td>
      <td>no</td>
      <td>unknown</td>
      <td>5</td>
      <td>may</td>
      <td>92</td>
      <td>1</td>
      <td>-1</td>
      <td>0</td>
      <td>unknown</td>
      <td>no</td>
    </tr>
    <tr>
      <th>4</th>
      <td>33</td>
      <td>unknown</td>
      <td>single</td>
      <td>unknown</td>
      <td>no</td>
      <td>1</td>
      <td>no</td>
      <td>no</td>
      <td>unknown</td>
      <td>5</td>
      <td>may</td>
      <td>198</td>
      <td>1</td>
      <td>-1</td>
      <td>0</td>
      <td>unknown</td>
      <td>no</td>
    </tr>
  </tbody>
</table>
</div>



Se quitan duplicados


```python
data=(
 data >>
    distinct()
)

(
 data >> head()
)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>age</th>
      <th>job</th>
      <th>marital</th>
      <th>education</th>
      <th>default</th>
      <th>balance</th>
      <th>housing</th>
      <th>loan</th>
      <th>contact</th>
      <th>day</th>
      <th>month</th>
      <th>duration</th>
      <th>campaign</th>
      <th>pdays</th>
      <th>previous</th>
      <th>poutcome</th>
      <th>y</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>58</td>
      <td>management</td>
      <td>married</td>
      <td>tertiary</td>
      <td>no</td>
      <td>2143</td>
      <td>yes</td>
      <td>no</td>
      <td>unknown</td>
      <td>5</td>
      <td>may</td>
      <td>261</td>
      <td>1</td>
      <td>-1</td>
      <td>0</td>
      <td>unknown</td>
      <td>no</td>
    </tr>
    <tr>
      <th>1</th>
      <td>44</td>
      <td>technician</td>
      <td>single</td>
      <td>secondary</td>
      <td>no</td>
      <td>29</td>
      <td>yes</td>
      <td>no</td>
      <td>unknown</td>
      <td>5</td>
      <td>may</td>
      <td>151</td>
      <td>1</td>
      <td>-1</td>
      <td>0</td>
      <td>unknown</td>
      <td>no</td>
    </tr>
    <tr>
      <th>2</th>
      <td>33</td>
      <td>entrepreneur</td>
      <td>married</td>
      <td>secondary</td>
      <td>no</td>
      <td>2</td>
      <td>yes</td>
      <td>yes</td>
      <td>unknown</td>
      <td>5</td>
      <td>may</td>
      <td>76</td>
      <td>1</td>
      <td>-1</td>
      <td>0</td>
      <td>unknown</td>
      <td>no</td>
    </tr>
    <tr>
      <th>3</th>
      <td>47</td>
      <td>blue-collar</td>
      <td>married</td>
      <td>unknown</td>
      <td>no</td>
      <td>1506</td>
      <td>yes</td>
      <td>no</td>
      <td>unknown</td>
      <td>5</td>
      <td>may</td>
      <td>92</td>
      <td>1</td>
      <td>-1</td>
      <td>0</td>
      <td>unknown</td>
      <td>no</td>
    </tr>
    <tr>
      <th>4</th>
      <td>33</td>
      <td>unknown</td>
      <td>single</td>
      <td>unknown</td>
      <td>no</td>
      <td>1</td>
      <td>no</td>
      <td>no</td>
      <td>unknown</td>
      <td>5</td>
      <td>may</td>
      <td>198</td>
      <td>1</td>
      <td>-1</td>
      <td>0</td>
      <td>unknown</td>
      <td>no</td>
    </tr>
  </tbody>
</table>
</div>



Información del DataFrame


```python
data.info()
```

    <class 'pandas.core.frame.DataFrame'>
    Int64Index: 45211 entries, 0 to 45210
    Data columns (total 17 columns):
    age          45211 non-null int64
    job          45211 non-null object
    marital      45211 non-null object
    education    45211 non-null object
    default      45211 non-null object
    balance      45211 non-null int64
    housing      45211 non-null object
    loan         45211 non-null object
    contact      45211 non-null object
    day          45211 non-null int64
    month        45211 non-null object
    duration     45211 non-null int64
    campaign     45211 non-null int64
    pdays        45211 non-null int64
    previous     45211 non-null int64
    poutcome     45211 non-null object
    y            45211 non-null object
    dtypes: int64(7), object(10)
    memory usage: 6.2+ MB


Se pone en 1 y 0 la variable **y**


```python
data=(
 data >>
    mutate(y = X.y.replace('yes', '1').replace('no', '0'))
)
```

## Descriptivos

Distribución de la edad


```python
(
 data >>
    ggplot() +
    geom_histogram(aes(x = 'age'), fill = 'darkblue', bins = 30) +
    theme_bw() +
    ggtitle('Histograma Edad') +
    ylab('Casos') +
    theme(axis_title_y=element_text(size=15),
          axis_title_x=element_text(size=15),
          axis_text_y=element_text(size=10),
          axis_text_x=element_text(size=10))
)
```


![png](output_15_0.png)





    <ggplot: (7554467981)>



Distribución de edad por trabajo


```python
(
 data >>
    ggplot() +
    geom_histogram(aes(x = 'age'), fill = 'darkblue', bins = 30) +
    theme_bw() +
    ggtitle('Histograma Edad') +
    ylab('Casos') +
    theme(axis_title_y=element_text(size=15),
          axis_title_x=element_text(size=15),
          axis_text_y=element_text(size=10),
          axis_text_x=element_text(size=10)) +
    facet_wrap('~job')
)
```


![png](output_17_0.png)





    <ggplot: (297701163)>



Distribución de trabajos


```python
jobs=(
 data >>
    group_by(X.job) >>
    summarize(n = n(X.job)) >>
    rename(conteo='n') >>
    arrange(X.conteo) >>
    select(X.job) >>
    pull
)

(
 data >>
    group_by(X.job) >>
    summarize(n = n(X.job)) >>
    rename(conteo='n') >>
    arrange(X.conteo) >>
    mutate(job = X.job.astype('category', order='F')) >>
    ggplot() +
    geom_bar(aes(x='job', y = 'conteo'), stat = 'identity', fill = 'darkblue') +
    ggtitle('Distribución trabajos') +
    theme_bw() +
    geom_text(aes(x='job', y='conteo', label='conteo'), size = 10, va = 'bottom') +
    theme(axis_text_x = element_text(angle = 90)) +
    scale_x_discrete(limits = jobs)
)
```


![png](output_19_0.png)





    <ggplot: (-9223372029299558947)>




```python
(
 data >>
    group_by(X.job) >>
    summarize(n = n(X.job)/len(data)*100) >>
    rename(pct='n') >>
    mutate(pct = X.pct.round(2)) >>
    arrange(X.pct) >>
    mutate(job = X.job.astype('category', order='F')) >>
    ggplot() +
    geom_bar(aes(x='job', y = 'pct'), stat = 'identity', fill = 'darkblue') +
    ggtitle('Distribución trabajos - %') +
    theme_bw() +
    theme(axis_text_x = element_text(angle = 90)) +
    scale_x_discrete(limits = jobs) +
    geom_text(aes(x='job', y='pct', label='pct'), size = 8, va = 'bottom', format_string='{}%')
)
```


![png](output_20_0.png)





    <ggplot: (-9223372029299412508)>



Distribución de trabajos por estado civil, quitando los trabajos desconocidos


```python
(
 data >>
    mask(X.job != 'unknown') >>
    group_by(X.job, X.marital) >>
    summarize(n = n(X.job)) >>
    group_by(X.marital) >>
    mutate(total = X.n.sum()) >>
    mutate(pct = X.n/X.total*100) >>
    mutate(pct = X.pct.round(2)) >>
    ggplot() +
    geom_bar(aes(x = 'job', y = 'pct'), stat = 'identity', fill = 'darkblue') +
    ggtitle('Distribución de trabajos por estado civil') +
    facet_wrap('~marital') +
    theme_bw() +
    theme(axis_text_x=element_text(angle = 90, size = 12, va = 'top'))
)
```


![png](output_22_0.png)





    <ggplot: (-9223372029299417526)>



Regresión Lineal de edad


```python
# Creamos data frame sólo con edad y la variable y, haciendo la y numérica
data_age=(
 data >>
    mutate(y = X.y.astype('int')) >>
    select(X.age, X.y)
)

# Dejamos la edad (que es la variable X) en un data frame
data_age_x=(
 data_age >>
    mutate(age2 = X.age*X.age) >>
    select(X.age, X.age2)
)

# Dejamos la variable y en un data frame separado
data_age_y=(
 data_age >>
    select(X.y)
)

# Creamos el objeto para hacer la regresión
regr = linear_model.LinearRegression()
```

Creamos el objeto para hacer la regresión


```python
regr = linear_model.LinearRegression()
```

Hacemos la regresión _fit_


```python
regr.fit(data_age_x, data_age_y)
```




    LinearRegression(copy_X=True, fit_intercept=True, n_jobs=1, normalize=False)



Se hace un predict a las mismas equis con las que se entrenó el modelo


```python
age_y_pred = regr.predict(data_age_x)
```

Se imprime el coeficente y el intercepto


```python
print('Coeficiente:', regr.coef_, '\nIntercepto', regr.intercept_)
```

    Coeficiente: [[-0.02787471  0.00032123]] 
    Intercepto [0.68353344]


Se crea un DataFrame con el coeficiente y el intercepto para graficarlos


```python
regr.coef_[0][1]
```




    0.0003212340982581569




```python
coefs={'Descripcion':['Coeficiente Edad', 'Coeficiente Edad_2', 'Intercepto'],'Coeficiente':[regr.coef_[0][0], regr.coef_[0][1], regr.intercept_[0]]}
coefs=pd.DataFrame(coefs)

orden_coef=(
 coefs >>
    arrange(X.Coeficiente) >>
    select('Descripcion') >>
    pull
)

(
 coefs >>
    arrange(X.Coeficiente) >>
    mutate(Coeficiente = X.Coeficiente.round(2)) >>
    ggplot() +
    geom_bar(aes(x='Descripcion',y='Coeficiente'),stat='identity',fill='darkblue') +
    scale_x_discrete(limits = orden_coef) +
    geom_text(aes(x='Descripcion', y='Coeficiente', label='Coeficiente'), va='bottom') +
    theme_bw()
)
```


![png](output_35_0.png)





    <ggplot: (7555178493)>



Se calcula el error cuadrático medio


```python
mean_squared_error(data_age_y, age_y_pred)
```




    0.10080627360132319



Se imprime la R2


```python
r2_score(data_age_y, age_y_pred)
```




    0.02413457814644593



Se crea grid para ver el efecto marginal


```python
grid=pd.DataFrame({'age':range(100)})
grid=(
 grid >>
    mutate(age2=X.age*X.age)
)

(
 grid >>
    head()
)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>age</th>
      <th>age2</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2</td>
      <td>4</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3</td>
      <td>9</td>
    </tr>
    <tr>
      <th>4</th>
      <td>4</td>
      <td>16</td>
    </tr>
  </tbody>
</table>
</div>



Se hacer predict al grid y se agrega al DataFrame con el _grid_ y se grafica


```python
grid_pred=regr.predict(grid)
(
 grid >>
    mutate(pred=grid_pred) >>
    ggplot() +
    geom_point(aes(x='age', y='pred')) +
    ggtitle('Efecto Marginal edad') +
    theme_bw()
)
```


![png](output_43_0.png)





    <ggplot: (-9223372029297439600)>



## Modelo H2O

Se instala H2O desde anaconda siguiendo las instrucciones del link:

https://h2o-release.s3.amazonaws.com/h2o/master/3888/docs-website/h2o-docs/downloading.html

También debe de instalarse JDK de Java para poder iniciar H2O

### Se inicia H2O


```python
import h2o
h2o.init(max_mem_size = "12G", nthreads=-1)
```

    Checking whether there is an H2O instance running at http://localhost:54321..... not found.
    Attempting to start a local H2O server...
      Java Version: java version "11.0.1" 2018-10-16 LTS; Java(TM) SE Runtime Environment 18.9 (build 11.0.1+13-LTS); Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11.0.1+13-LTS, mixed mode)
      Starting server from /Users/darias/anaconda3/lib/python3.7/site-packages/h2o/backend/bin/h2o.jar
      Ice root: /var/folders/dz/y365nyjn6kqgc1k6dc68xkg00000gp/T/tmp3dy2mit6
      JVM stdout: /var/folders/dz/y365nyjn6kqgc1k6dc68xkg00000gp/T/tmp3dy2mit6/h2o_darias_started_from_python.out
      JVM stderr: /var/folders/dz/y365nyjn6kqgc1k6dc68xkg00000gp/T/tmp3dy2mit6/h2o_darias_started_from_python.err
      Server is running at http://127.0.0.1:54321
    Connecting to H2O server at http://127.0.0.1:54321... successful.



<div style="overflow:auto"><table style="width:50%"><tr><td>H2O cluster uptime:</td>
<td>01 secs</td></tr>
<tr><td>H2O cluster timezone:</td>
<td>America/Mexico_City</td></tr>
<tr><td>H2O data parsing timezone:</td>
<td>UTC</td></tr>
<tr><td>H2O cluster version:</td>
<td>3.22.0.2</td></tr>
<tr><td>H2O cluster version age:</td>
<td>25 days </td></tr>
<tr><td>H2O cluster name:</td>
<td>H2O_from_python_darias_3oz75f</td></tr>
<tr><td>H2O cluster total nodes:</td>
<td>1</td></tr>
<tr><td>H2O cluster free memory:</td>
<td>12 Gb</td></tr>
<tr><td>H2O cluster total cores:</td>
<td>12</td></tr>
<tr><td>H2O cluster allowed cores:</td>
<td>12</td></tr>
<tr><td>H2O cluster status:</td>
<td>accepting new members, healthy</td></tr>
<tr><td>H2O connection url:</td>
<td>http://127.0.0.1:54321</td></tr>
<tr><td>H2O connection proxy:</td>
<td>None</td></tr>
<tr><td>H2O internal security:</td>
<td>False</td></tr>
<tr><td>H2O API Extensions:</td>
<td>XGBoost, Algos, AutoML, Core V3, Core V4</td></tr>
<tr><td>Python version:</td>
<td>3.7.0 final</td></tr></table></div>


### Cambio de string a categorías

Se convierte a H2O el _Data frame_, convirtiendo en categoría primero las columnas necesarias


```python
data=(
 data >>
    mutate(job       = X.job.astype('category'),
           marital   = X.marital.astype('category'),
           education = X.education.astype('category'),
           default   = X.default.astype('category'),
           housing   = X.housing.astype('category'),
           loan      = X.loan.astype('category'),
           contact   = X.contact.astype('category'),
           month     = X.month.astype('category'),
           poutcome  = X.poutcome.astype('category'),
           y         = X.y.astype('category'))    
)
```

### Datos a H2O


```python
datos_h2o=h2o.H2OFrame(data)
```

    Parse progress: |█████████████████████████████████████████████████████████| 100%


### Categorías a factores


```python
datos_h2o['job']=datos_h2o['job'].asfactor()
datos_h2o['marital']=datos_h2o['marital'].asfactor()
datos_h2o['education']=datos_h2o['education'].asfactor()
datos_h2o['default']=datos_h2o['default'].asfactor()
datos_h2o['housing']=datos_h2o['housing'].asfactor()
datos_h2o['loan']=datos_h2o['loan'].asfactor()
datos_h2o['contact']=datos_h2o['contact'].asfactor()
datos_h2o['month']=datos_h2o['month'].asfactor()
datos_h2o['poutcome']=datos_h2o['poutcome'].asfactor()
datos_h2o['y']=datos_h2o['y'].asfactor()
```

### Split de datos

Se hace el _split_ en:
  - train
  - test
  - validation


```python
train, valid, test = datos_h2o.split_frame(ratios=[0.6,0.2], seed=1234)
```

### Se definen X y Y

Se separan las variables X y Y


```python
varsX=datos_h2o.col_names[:-1]
varsX=varsX[:-5]
varsY=datos_h2o.col_names[-1]
```


```python
varsX
```




    ['age',
     'job',
     'marital',
     'education',
     'default',
     'balance',
     'housing',
     'loan',
     'contact',
     'day',
     'month']




```python
varsY
```




    'y'



### Entrenamiento - grid

Entrenamiento


```python
from h2o.estimators.gbm import H2OGradientBoostingEstimator
from h2o.grid.grid_search import H2OGridSearch
```

Se crea el grid de hiperparámetros


```python
hyper_parameters = {'learn_rate': [0.008, 0.01, 0.02],
                    'max_depth': [6,7,8,9],
                    'sample_rate': [0,6,0,7,0.8,0.9],
                    'col_sample_rate': [0.5, 0.6, 0,7, 1]}

search_criteria = { 'strategy': "RandomDiscrete",
                    'seed': 42,
                    'stopping_metric': "AUC", 
                    'stopping_tolerance': 0.001,
                    'stopping_rounds': 5 }
```


```python
grid_gbm=H2OGridSearch(H2OGradientBoostingEstimator(),
                       hyper_parameters,
                       grid_id="random_plus_manual",
                       search_criteria=search_criteria)
```


```python
grid_gbm.train(x=varsX,
               y=varsY,
               training_frame=train,
               validation_frame=test,
               ntrees=200,
               seed=1,
               nfolds=4,
               balance_classes=True)
```

    gbm Grid Build progress: |████████████████████████████████████████████████| 100%
    Errors/Warnings building gridsearch model
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_1.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_1.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_2.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_2.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_3.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_3.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_4.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_4.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_6.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_6.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_7.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_7.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_8.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_8.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_10.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_10.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_11.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_11.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_13.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_13.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_14.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_14.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_15.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_15.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_16.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_16.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_17.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_17.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_19.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_19.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_20.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_20.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_21.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_21.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_22.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_22.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_23.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_23.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_24.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_24.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_26.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_26.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_27.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_27.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_29.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_29.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_30.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_30.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_31.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_31.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_32.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_32.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_33.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_33.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_34.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_34.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_36.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_36.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_37.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_37.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_38.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_38.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_39.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_39.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_40.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_40.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_41.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_41.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_42.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_42.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_43.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_43.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_44.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_44.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_45.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_45.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_47.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_47.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_48.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_48.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_49.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_49.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_50.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_50.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_52.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_52.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_53.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_53.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_55.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_55.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_56.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_56.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_57.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_57.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_58.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_58.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_60.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_60.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_62.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_62.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_63.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_63.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_64.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_64.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_66.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_66.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_67.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_67.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_68.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_68.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_69.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_69.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_70.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_70.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_71.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_71.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_72.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_72.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_74.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_74.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_75.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_75.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_76.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_76.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_77.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_77.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_79.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_79.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_80.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_80.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_81.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_81.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_83.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_83.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_84.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_84.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_86.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_86.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_87.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_87.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_88.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_88.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_89.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_89.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_90.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_90.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_91.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_91.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_92.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_92.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_93.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_93.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_94.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_94.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_95.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_95.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_96.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_96.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_97.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_97.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_98.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_98.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_99.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_99.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_100.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_100.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_101.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_101.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_103.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_103.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_104.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_104.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_105.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_105.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_107.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_107.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_108.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_108.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_109.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_109.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_110.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_110.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_111.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_111.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_112.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_112.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_113.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_113.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_114.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_114.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_115.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_115.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_116.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_116.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_117.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_117.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_120.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_120.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_122.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_122.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_123.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_123.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_124.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_124.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_125.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_125.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_127.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_127.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_130.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_130.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_131.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_131.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_132.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_132.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_134.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_134.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_135.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_135.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_136.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_136.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_137.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_137.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_138.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_138.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_139.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_139.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_143.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_143.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_144.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_144.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_145.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_145.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_146.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_146.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_147.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_147.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_148.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_148.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_149.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_149.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_150.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_150.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_152.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_152.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_153.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_153.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_154.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_154.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_155.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_155.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_156.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_156.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_162.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_162.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_163.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_163.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_164.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_164.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_165.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_165.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_166.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_166.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_168.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_168.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_169.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_169.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_170.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_170.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_171.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_171.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_172.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_172.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_173.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_173.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_174.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_174.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_175.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_175.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_176.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_176.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_178.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_178.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_179.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_179.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_180.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_180.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_181.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_181.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_183.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_183.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_184.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_184.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_185.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_185.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_186.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_186.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_187.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_187.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_190.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_190.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_191.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_191.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_192.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_192.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_193.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_193.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_194.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_194.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_195.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_195.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_196.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_196.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_197.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_197.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_198.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_198.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_199.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_199.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_200.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_200.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_201.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_201.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_202.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_202.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_203.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_203.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_204.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_204.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_205.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_205.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_206.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_206.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_207.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_207.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_209.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_209.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_210.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_210.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_211.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_211.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_212.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_212.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_213.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_213.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_214.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_214.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_216.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_216.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_217.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_217.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_219.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_219.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_220.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_220.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_221.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_221.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_222.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_222.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_225.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_225.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_226.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_226.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_227.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_227.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_228.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_228.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_229.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_229.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_230.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_230.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_231.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_231.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_233.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_233.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_234.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_234.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_235.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_235.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_236.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_236.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_239.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_239.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_240.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_240.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_241.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_241.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_242.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_242.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_243.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_243.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_246.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_246.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_247.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_247.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_248.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_248.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_249.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_249.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_250.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_250.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_251.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_251.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_252.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_252.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_253.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_253.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_254.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_254.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_255.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_255.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_256.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_256.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_257.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_257.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_258.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_258.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_259.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_259.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_260.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_260.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_261.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_261.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_262.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_262.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_264.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_264.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_265.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_265.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_266.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_266.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_267.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_267.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_269.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_269.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_271.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_271.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_273.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_273.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_274.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_274.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_275.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_275.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_276.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_276.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_277.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_277.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_278.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_278.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_280.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_280.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_281.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_281.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_282.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_282.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_283.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_283.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_284.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_284.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_285.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_285.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_286.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_286.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_288.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_288.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_289.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_289.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_290.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_290.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_291.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_291.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_294.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_294.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_295.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_295.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_296.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_296.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_297.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_297.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_298.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_298.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_299.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_299.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_300.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_300.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_302.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_302.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_303.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_303.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_304.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_304.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_305.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_305.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_306.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_306.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_307.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_307.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_308.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_308.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_309.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_309.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_311.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_311.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_312.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_312.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.9
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_313.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_313.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_317.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_317.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.5
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_318.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_318.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_319.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_319.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_320.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_320.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_321.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_321.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_322.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_322.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_323.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_323.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_324.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_324.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_325.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_325.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.8
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_326.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_326.  Details: ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_328.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_328.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_329.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_329.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_330.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_330.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 7
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_331.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_331.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_332.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_332.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 6.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_333.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_333.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 6.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 7.0
    Hyper-parameter: learn_rate, 0.02
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_334.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_334.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _col_sample_rate: col_sample_rate must be between 0 and 1
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.008
    Hyper-parameter: max_depth, 9
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_337.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_337.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 1.0
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 0.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_338.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_338.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 0.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 6
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_340.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_340.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    
    Hyper-parameter: col_sample_rate, 0.6
    Hyper-parameter: learn_rate, 0.01
    Hyper-parameter: max_depth, 8
    Hyper-parameter: sample_rate, 7.0
    failure_details: Illegal argument(s) for GBM model: random_plus_manual_model_341.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    failure_stack_traces: water.exceptions.H2OModelBuilderIllegalArgumentException: Illegal argument(s) for GBM model: random_plus_manual_model_341.  Details: ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    ERRR on field: _sample_rate: sample_rate should be in interval ]0,1] but it is 7.0.
    
    	at water.exceptions.H2OModelBuilderIllegalArgumentException.makeFromBuilder(H2OModelBuilderIllegalArgumentException.java:20)
    	at hex.ModelBuilder.cv_makeFramesAndBuilders(ModelBuilder.java:537)
    	at hex.ModelBuilder.computeCrossValidation(ModelBuilder.java:380)
    	at hex.ModelBuilder.trainModelNested(ModelBuilder.java:330)
    	at hex.grid.GridSearch.startBuildModel(GridSearch.java:360)
    	at hex.grid.GridSearch.buildModel(GridSearch.java:342)
    	at hex.grid.GridSearch.gridSearch(GridSearch.java:220)
    	at hex.grid.GridSearch.access$000(GridSearch.java:70)
    	at hex.grid.GridSearch$1.compute2(GridSearch.java:137)
    	at water.H2O$H2OCountedCompleter.compute(H2O.java:1310)
    	at jsr166y.CountedCompleter.exec(CountedCompleter.java:468)
    	at jsr166y.ForkJoinTask.doExec(ForkJoinTask.java:263)
    	at jsr166y.ForkJoinPool$WorkQueue.runTask(ForkJoinPool.java:974)
    	at jsr166y.ForkJoinPool.runWorker(ForkJoinPool.java:1477)
    	at jsr166y.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:104)
    
    


Se ven los resultados del grid

### Resultados Grid


```python
grid_gbm_list=grid_gbm.get_grid(sort_by='logloss', decreasing=False)
grid_gbm_list
```

          col_sample_rate learn_rate max_depth sample_rate  \
    0                 0.6       0.02         6         0.8   
    1                 0.5       0.02         6         0.8   
    2                 0.6       0.02         6         0.9   
    3                 0.5       0.02         6         0.9   
    4                 1.0       0.02         6         0.9   
    5                 0.5       0.02         7         0.8   
    6                 0.5       0.02         7         0.9   
    7                 0.6       0.02         7         0.8   
    8                 0.6       0.02         7         0.9   
    9                 1.0       0.02         7         0.8   
    10                1.0       0.02         7         0.9   
    11                0.5       0.02         8         0.8   
    12                0.6       0.01         7         0.8   
    13                0.6       0.02         8         0.8   
    14                0.6       0.01         8         0.8   
    15                0.5       0.02         8         0.9   
    16                0.6       0.01         7         0.9   
    17                1.0       0.01         7         0.8   
    18                0.5       0.01         7         0.8   
    19                0.5       0.01         8         0.8   
    20                0.6       0.01         8         0.9   
    21                1.0       0.01         6         0.8   
    22                0.6       0.02         8         0.9   
    23                0.5       0.01         7         0.9   
    24                0.6       0.01         6         0.8   
    25                0.5       0.01         8         0.9   
    26                1.0       0.01         7         0.9   
    27                0.6       0.01         6         0.9   
    28                1.0       0.01         6         0.9   
    29                0.5       0.01         9         0.8   
    .. ..             ...        ...       ...         ...   
    38                0.6      0.008         8         0.8   
    39                1.0       0.01         9         0.8   
    40                1.0       0.02         8         0.9   
    41                0.5      0.008         8         0.8   
    42                0.6      0.008         8         0.9   
    43                0.6      0.008         7         0.8   
    44                1.0      0.008         7         0.8   
    45                0.5      0.008         8         0.9   
    46                0.6      0.008         7         0.9   
    47                0.5       0.02         9         0.8   
    48                0.6      0.008         9         0.8   
    49                0.5      0.008         9         0.8   
    50                1.0      0.008         8         0.8   
    51                0.5      0.008         7         0.8   
    52                0.5      0.008         7         0.9   
    53                0.6      0.008         9         0.9   
    54                1.0       0.01         9         0.9   
    55                1.0      0.008         6         0.9   
    56                0.5      0.008         9         0.9   
    57                0.6      0.008         6         0.8   
    58                0.6      0.008         6         0.9   
    59                1.0      0.008         9         0.8   
    60                0.5       0.02         9         0.9   
    61                0.5      0.008         6         0.8   
    62                0.6       0.02         9         0.8   
    63                0.5      0.008         6         0.9   
    64                1.0      0.008         9         0.9   
    65                0.6       0.02         9         0.9   
    66                1.0       0.02         9         0.8   
    67                1.0       0.02         9         0.9   
    
                           model_ids              logloss  
    0    random_plus_manual_model_78  0.30046482049851103  
    1   random_plus_manual_model_133  0.30076631483379695  
    2   random_plus_manual_model_119  0.30094561142663584  
    3    random_plus_manual_model_51  0.30107123045413003  
    4   random_plus_manual_model_272   0.3012758708559564  
    5    random_plus_manual_model_28   0.3015132972924245  
    6   random_plus_manual_model_263  0.30163133888498656  
    7   random_plus_manual_model_167  0.30163672449269835  
    8   random_plus_manual_model_218  0.30219127834610976  
    9   random_plus_manual_model_327   0.3029277048179443  
    10   random_plus_manual_model_35   0.3034318575662325  
    11  random_plus_manual_model_188   0.3038252783993316  
    12  random_plus_manual_model_342  0.30416284680235545  
    13  random_plus_manual_model_292  0.30423884672954904  
    14  random_plus_manual_model_182  0.30426476091062876  
    15  random_plus_manual_model_314  0.30431439649583575  
    16  random_plus_manual_model_336  0.30449383287692977  
    17  random_plus_manual_model_301   0.3045189116508627  
    18  random_plus_manual_model_237   0.3045988783527479  
    19  random_plus_manual_model_151   0.3046152000646947  
    20  random_plus_manual_model_102   0.3047249105728375  
    21  random_plus_manual_model_287   0.3047528352889096  
    22  random_plus_manual_model_159    0.304779596372608  
    23  random_plus_manual_model_316   0.3048303460756992  
    24  random_plus_manual_model_339   0.3049728255075806  
    25  random_plus_manual_model_160  0.30497878806513723  
    26  random_plus_manual_model_310  0.30508955137233107  
    27   random_plus_manual_model_46  0.30510771172750617  
    28  random_plus_manual_model_128  0.30516454467297255  
    29  random_plus_manual_model_158  0.30531632879365656  
    ..                           ...                  ...  
    38   random_plus_manual_model_54   0.3067681776167557  
    39  random_plus_manual_model_126   0.3069261337781885  
    40   random_plus_manual_model_65  0.30697222853505346  
    41  random_plus_manual_model_232   0.3070012969807315  
    42  random_plus_manual_model_238  0.30701587834062305  
    43  random_plus_manual_model_208  0.30705031402326594  
    44   random_plus_manual_model_25   0.3071937193080607  
    45  random_plus_manual_model_224  0.30727181758963873  
    46  random_plus_manual_model_244   0.3073023396478541  
    47  random_plus_manual_model_335   0.3073094891687577  
    48  random_plus_manual_model_270   0.3073995240208135  
    49   random_plus_manual_model_85  0.30748715148147476  
    50  random_plus_manual_model_245   0.3074992795968348  
    51  random_plus_manual_model_268   0.3075382823355609  
    52   random_plus_manual_model_18  0.30773781397167654  
    53    random_plus_manual_model_5  0.30780951143315066  
    54  random_plus_manual_model_106  0.30782717663031933  
    55  random_plus_manual_model_293  0.30783632786479365  
    56   random_plus_manual_model_12   0.3078539537532898  
    57  random_plus_manual_model_142   0.3080851166834693  
    58   random_plus_manual_model_73  0.30821124457108817  
    59  random_plus_manual_model_118  0.30849345643557863  
    60    random_plus_manual_model_9  0.30855367520517174  
    61  random_plus_manual_model_140  0.30871504908678565  
    62  random_plus_manual_model_129   0.3087347161476026  
    63  random_plus_manual_model_141   0.3088918123586784  
    64  random_plus_manual_model_177  0.30928125106187565  
    65  random_plus_manual_model_223  0.30967205326666747  
    66   random_plus_manual_model_82  0.31156595825314737  
    67  random_plus_manual_model_189   0.3118387313034382  
    
    [68 rows x 7 columns]





    



### Mejor Modelo

Se extrae el mejor modelo


```python
best_gbm = grid_gbm_list.models[0]
```

Se guarda el mejor modelo


```python
h2o.save_model(model=best_gbm,
               path="/Users/darias/Documents/Aprendiendo_python/modelo_pyhton/Modelo_gbm",
               force=True)
```




    '/Users/darias/Documents/Aprendiendo_python/modelo_pyhton/Modelo_gbm/random_plus_manual_model_78'



Se carga el modelo _(si es que se requiere, cuando se reinicia el kernel)_


```python
#best_gbm = h2o.load_model('/Users/darias/Documents/Aprendiendo_python/modelo_pyhton/Modelo_gbm/random_plus_manual_model_65')
```

### Importancia de variables

Importancia de variables


```python
imp=best_gbm.varimp()
imp=pd.DataFrame(imp)
imp
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>month</td>
      <td>31547.699219</td>
      <td>1.000000</td>
      <td>0.354093</td>
    </tr>
    <tr>
      <th>1</th>
      <td>contact</td>
      <td>13221.429688</td>
      <td>0.419093</td>
      <td>0.148398</td>
    </tr>
    <tr>
      <th>2</th>
      <td>day</td>
      <td>11133.833008</td>
      <td>0.352921</td>
      <td>0.124967</td>
    </tr>
    <tr>
      <th>3</th>
      <td>job</td>
      <td>8246.935547</td>
      <td>0.261412</td>
      <td>0.092564</td>
    </tr>
    <tr>
      <th>4</th>
      <td>housing</td>
      <td>7184.817383</td>
      <td>0.227745</td>
      <td>0.080643</td>
    </tr>
    <tr>
      <th>5</th>
      <td>age</td>
      <td>7170.170898</td>
      <td>0.227280</td>
      <td>0.080478</td>
    </tr>
    <tr>
      <th>6</th>
      <td>balance</td>
      <td>4937.459961</td>
      <td>0.156508</td>
      <td>0.055418</td>
    </tr>
    <tr>
      <th>7</th>
      <td>marital</td>
      <td>2015.820801</td>
      <td>0.063898</td>
      <td>0.022626</td>
    </tr>
    <tr>
      <th>8</th>
      <td>education</td>
      <td>1802.825806</td>
      <td>0.057146</td>
      <td>0.020235</td>
    </tr>
    <tr>
      <th>9</th>
      <td>loan</td>
      <td>1568.062256</td>
      <td>0.049704</td>
      <td>0.017600</td>
    </tr>
    <tr>
      <th>10</th>
      <td>default</td>
      <td>265.267700</td>
      <td>0.008408</td>
      <td>0.002977</td>
    </tr>
  </tbody>
</table>
</div>




```python
imp=(
 imp >>
    rename(variable            = 0,
           relative_importance = 1,
           scaled_importance   = 2,
           percentage          = 3)
)

imp
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>variable</th>
      <th>relative_importance</th>
      <th>scaled_importance</th>
      <th>percentage</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>month</td>
      <td>31547.699219</td>
      <td>1.000000</td>
      <td>0.354093</td>
    </tr>
    <tr>
      <th>1</th>
      <td>contact</td>
      <td>13221.429688</td>
      <td>0.419093</td>
      <td>0.148398</td>
    </tr>
    <tr>
      <th>2</th>
      <td>day</td>
      <td>11133.833008</td>
      <td>0.352921</td>
      <td>0.124967</td>
    </tr>
    <tr>
      <th>3</th>
      <td>job</td>
      <td>8246.935547</td>
      <td>0.261412</td>
      <td>0.092564</td>
    </tr>
    <tr>
      <th>4</th>
      <td>housing</td>
      <td>7184.817383</td>
      <td>0.227745</td>
      <td>0.080643</td>
    </tr>
    <tr>
      <th>5</th>
      <td>age</td>
      <td>7170.170898</td>
      <td>0.227280</td>
      <td>0.080478</td>
    </tr>
    <tr>
      <th>6</th>
      <td>balance</td>
      <td>4937.459961</td>
      <td>0.156508</td>
      <td>0.055418</td>
    </tr>
    <tr>
      <th>7</th>
      <td>marital</td>
      <td>2015.820801</td>
      <td>0.063898</td>
      <td>0.022626</td>
    </tr>
    <tr>
      <th>8</th>
      <td>education</td>
      <td>1802.825806</td>
      <td>0.057146</td>
      <td>0.020235</td>
    </tr>
    <tr>
      <th>9</th>
      <td>loan</td>
      <td>1568.062256</td>
      <td>0.049704</td>
      <td>0.017600</td>
    </tr>
    <tr>
      <th>10</th>
      <td>default</td>
      <td>265.267700</td>
      <td>0.008408</td>
      <td>0.002977</td>
    </tr>
  </tbody>
</table>
</div>




```python
(
 imp >>
    mutate(scaled_importance = X.scaled_importance*100) >>
    mutate(scaled_importance = X.scaled_importance.round(1)) >>
    ggplot() +
    geom_bar(aes(x='variable', y='scaled_importance'), stat='identity', fill='darkblue') +
    ggtitle('Importancia de variables') +
    scale_x_discrete(limits = imp >> arrange(X.relative_importance) >> select(X.variable) >> pull) +
    theme(axis_text_x=element_text(angle=90, face='bold', size=10),
          axis_text_y=element_text(face='bold', size=10)) +
    theme_bw() +
    coord_flip() +
    geom_text(aes(x='variable',y='scaled_importance',label='scaled_importance'), ha='left', format_string='{}%', size = 10)
)
```


![png](output_85_0.png)





    <ggplot: (7577939881)>



### Efectos Marginales del Top 3

Se obtienen las gráficas del Top 3 de importancia de variables


```python
df_pp_1=best_gbm.partial_plot(cols=['month'],data=valid,plot=False)
```

    PartialDependencePlot progress: |█████████████████████████████████████████| 100%



```python
df_pp_1=df_pp_1[0].as_data_frame()
(
 df_pp_1 >>
    head()
)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>month</th>
      <th>mean_response</th>
      <th>stddev_response</th>
      <th>std_error_mean_response</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>apr</td>
      <td>0.205472</td>
      <td>0.124489</td>
      <td>0.001308</td>
    </tr>
    <tr>
      <th>1</th>
      <td>aug</td>
      <td>0.100516</td>
      <td>0.060610</td>
      <td>0.000637</td>
    </tr>
    <tr>
      <th>2</th>
      <td>dec</td>
      <td>0.296465</td>
      <td>0.147755</td>
      <td>0.001553</td>
    </tr>
    <tr>
      <th>3</th>
      <td>feb</td>
      <td>0.182249</td>
      <td>0.127500</td>
      <td>0.001340</td>
    </tr>
    <tr>
      <th>4</th>
      <td>jan</td>
      <td>0.134148</td>
      <td>0.098548</td>
      <td>0.001036</td>
    </tr>
  </tbody>
</table>
</div>




```python
df_pp_1=(
 df_pp_1 >>
    arrange(X.mean_response)
)
```


```python
(
 df_pp_1 >>
    mutate(mean_response=(X.mean_response*100).round(2)) >>
    ggplot() +
    geom_bar(aes(x = 'month', y = 'mean_response'),stat='identity', fill = 'darkblue') +
    theme_bw() +
    ggtitle('Efecto Marginal - Mes') +
    theme(axis_text_x=element_text(angle=90,size=13),
          axis_text_y=element_text(size=13),
          axis_title_x=element_text(size=13),
          axis_title_y=element_text(size=13)) +
    scale_x_discrete(limits=df_pp_1>>select(X.month)>>pull) +
    geom_text(aes(x='month', y='mean_response',label='mean_response'), va='bottom',size=10)
)
```


![png](output_91_0.png)





    <ggplot: (-9223372029276811246)>




```python
df_pp_2=best_gbm.partial_plot(cols=['contact'],data=valid,plot=False)
df_pp_2=df_pp_2[0].as_data_frame()

df_pp_2=(
 df_pp_2 >>
    arrange(X.mean_response)
)

(
 df_pp_2 >>
    mutate(mean_response=(X.mean_response*100).round(2)) >>
    ggplot() +
    geom_bar(aes(x = 'contact', y = 'mean_response'),stat='identity', fill = 'darkblue') +
    theme_bw() +
    ggtitle('Efecto Marginal - Canal contacto') +
    theme(axis_text_x=element_text(angle=90,size=13),
          axis_text_y=element_text(size=13),
          axis_title_x=element_text(size=13),
          axis_title_y=element_text(size=13)) +
    scale_x_discrete(limits=df_pp_2>>select(X.contact)>>pull) +
    geom_text(aes(x='contact', y='mean_response',label='mean_response'), va='bottom',size=10)
)
```

    PartialDependencePlot progress: |█████████████████████████████████████████| 100%



![png](output_92_1.png)





    <ggplot: (7578158554)>




```python
df_pp_3=best_gbm.partial_plot(cols=['day'],data=valid,plot=False)
df_pp_3=df_pp_3[0].as_data_frame()

df_pp_3=(
 df_pp_3 >>
    arrange(X.mean_response)
)

(
 df_pp_3 >>
    mutate(mean_response=(X.mean_response*100).round(2)) >>
    ggplot() +
    geom_point(aes(x = 'day', y = 'mean_response'), colour = 'darkblue') +
    geom_line(aes(x = 'day', y = 'mean_response', group = 1), colour = 'darkblue') +
    theme_bw() +
    ggtitle('Efecto Marginal - Día') +
    theme(axis_text_x=element_text(angle=90,size=13),
          axis_text_y=element_text(size=13),
          axis_title_x=element_text(size=13),
          axis_title_y=element_text(size=13))
)
```

    PartialDependencePlot progress: |█████████████████████████████████████████| 100%


    /Users/darias/anaconda3/lib/python3.7/site-packages/plotnine/layer.py:517: MatplotlibDeprecationWarning: isinstance(..., numbers.Number)
      return not cbook.iterable(value) and (cbook.is_numlike(value) or



![png](output_93_2.png)





    <ggplot: (-9223372029276818379)>



### Performance


```python
perf_train=best_gbm.model_performance(train)
perf_test=best_gbm.model_performance(test)
perf_valid=best_gbm.model_performance(valid)
```


```python
ll={'desc':['Train', 'test', 'valid'],'Coeficiente':[perf_train.logloss(),
                                                     perf_test.logloss(),
                                                     perf_valid.logloss()]}
ll=pd.DataFrame(ll)

orden_ll=(
 ll >>
    select('desc') >>
    pull
)

(
 ll >>
    mutate(Coeficiente = X.Coeficiente.round(2)) >>
    ggplot() +
    geom_bar(aes(x='desc',y='Coeficiente'),stat='identity',fill='darkblue') +
    scale_x_discrete(limits = orden_ll) +
    geom_text(aes(x='desc', y='Coeficiente', label='Coeficiente'), va='bottom') +
    theme_bw() +
    ggtitle('Comparación Logloss')
)
```


![png](output_96_0.png)





    <ggplot: (7578254870)>




```python
df_auc={'desc':['Train', 'test', 'valid'],'Coeficiente':[perf_train.auc(),
                                                         perf_test.auc(),
                                                         perf_valid.auc()]}
df_auc=pd.DataFrame(df_auc)

orden_df_auc=(
 ll >>
    select('desc') >>
    pull
)

(
 df_auc >>
    mutate(Coeficiente = X.Coeficiente.round(2)) >>
    ggplot() +
    geom_bar(aes(x='desc',y='Coeficiente'),stat='identity',fill='darkblue') +
    scale_x_discrete(limits = orden_df_auc) +
    geom_text(aes(x='desc', y='Coeficiente', label='Coeficiente'), va='bottom') +
    theme_bw() +
    ggtitle('Comparación AUC')
)
```


![png](output_97_0.png)





    <ggplot: (-9223372029276833115)>



### Efectividades

Se obtiene el _score_ de _valid_


```python
est_valid=best_gbm.predict(valid)
est_valid=est_valid.as_data_frame()
est_valid=est_valid['p1']
```

    gbm prediction progress: |████████████████████████████████████████████████| 100%


Se obtiene el _observado_ de _valid_


```python
obs_valid=valid['y']
obs_valid=obs_valid.as_data_frame()
```

Se juntan estimado y observado en un mismo data frame


```python
obs_est=(
 obs_valid >>
    mutate(est = est_valid) >>
    rename(obs='y')
)

(
 obs_est >>
    head()
)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>obs</th>
      <th>est</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0</td>
      <td>0.032250</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0</td>
      <td>0.023729</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0</td>
      <td>0.018935</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0</td>
      <td>0.022207</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0</td>
      <td>0.026102</td>
    </tr>
  </tbody>
</table>
</div>



Se agregan los percentiles


```python
obs_est['per']=pd.qcut(obs_est.est, q=[0.01,0.1,0.2,0.3,0.4,0.5,
                                       0.6,0.7,0.8,0.9,1.0], 
                                   labels=[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0])
```

Se obtienen las efectividades por percentiles


```python
pct_1=(
 obs_est >>
    mutate(obs = X.obs.astype('int')) >>
    summarize(pct = X.obs.sum()/len(obs_est)) >>
    pull
)

(
 obs_est >>
    mutate(obs = X.obs.astype('int')) >>
    group_by(X.per) >>
    summarize(n = n(X.obs),
              vp = X.obs.sum()) >>
    mutate(eff = X.vp/X.n*100) >>
    mutate(eff = X.eff.round(2)) >>
    ggplot() +
    geom_bar(aes(x = 'per', y = 'eff'), stat = 'identity', fill = 'darkblue') +
    theme_bw() +
    ggtitle('Efectividades por percentiles') +
    theme(axis_text_x  = element_text(size = 13, face = 'bold'),
          axis_text_y  = element_text(size = 13, face = 'bold'),
          axis_title_x = element_text(size = 13, face = 'bold'),
          axis_title_y = element_text(size = 13, face = 'bold')) +
    geom_text(aes(x='per',y='eff',label='eff'),va='bottom',size=8,format_string='{}%') +
    geom_hline(yintercept=pct_1*100) +
    annotate(geom  = 'text',
             x     = 0.8,
             y     = pct_1*100,
             label = 'Efect promedio: '+(100*pct_1).round(2).astype('str')+'%',
             ha    = 'left',
             va    = 'bottom')
)
```


![png](output_108_0.png)





    <ggplot: (7577942592)>




```python
h2o.shutdown()
```

    [WARNING] in <ipython-input-58-1edf85295eae> line 1:
        >>> h2o.shutdown()
            ^^^^ Deprecated, use ``h2o.cluster().shutdown()``.
    H2O session _sid_a3fe closed.

